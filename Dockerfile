### Docker file
## Author: Sergey Petrovsky

FROM library/openjdk:8u111-jdk-alpine
VOLUME /data
COPY ./target/route-service.jar /route-service.jar
COPY ./data/bus-routes.txt /data/bus-routes.txt
EXPOSE 8088
CMD ["java", "-jar", "route-service.jar"]