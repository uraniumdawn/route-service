package com.petrovsky.rs.controller;

import com.petrovsky.rs.MainApplication;
import com.petrovsky.rs.conf.properties.Properties;
import com.petrovsky.rs.controller.RouteController;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Test class for Route Controller
 *
 * @author Sergey Petrovsky
*/
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class RouteControllerTest {

    @Autowired
    private MockMvc mvc;

    @Test
    public void getDirectBusRouteTest() throws Exception {
        mvc
                .perform(get("/api/direct").param("dep_sid", "3").param("arr_sid", "6"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.dep_sid", is(3)))
                .andExpect(jsonPath("$.arr_sid", is(6)))
                .andExpect(jsonPath("$.direct_bus_route", is(true)));
    }

    @Test
    public void getDirectBusRouteWithSameIdTest() throws Exception {
        mvc
                .perform(get("/api/direct").param("dep_sid", "2").param("arr_sid", "6"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.dep_sid", is(2)))
                .andExpect(jsonPath("$.arr_sid", is(6)))
                .andExpect(jsonPath("$.direct_bus_route", is(false)));
    }

}
