package com.petrovsky.rs.controller;

import com.petrovsky.rs.model.Response;
import com.petrovsky.rs.service.RouteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.util.MimeType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

/**
 * Class RouteController
 * REST Controller that handles all web requests
 *
 * @author Sergey Petrovsky
 */
@RestController
public class RouteController {

    @Autowired
    private RouteService routeService;

    @RequestMapping(value = "/api/direct", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public Response getDirectBusRoute(HttpServletRequest httpServletRequest, @RequestParam("dep_sid") Integer departureID,
                                      @RequestParam("arr_sid") Integer arrivalID) {
        return routeService.getDirectBusRoute(departureID, arrivalID);
    }
}
