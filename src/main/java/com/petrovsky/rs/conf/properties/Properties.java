package com.petrovsky.rs.conf.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Properties
 * Properties class for application that handle all properties
 *
 * @author Sergey Petrovsky
 */
@Component
@ConfigurationProperties(prefix = "data")
public class Properties {
    private String filePath;

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }
}
