package com.petrovsky.rs.service.functional;

import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Functions
 * Class consists exclusively of static factory methods that provides functionality in functional style
 *
 * @author Sergey Petrovsky
 */
public class Functions {

    /**
     * Function provides parsing data routes through station ids
     */
    public static Function<String, List<Integer>> getRouteParser() {
        return l -> Stream.of(l.split(" "))
                .map(Integer::parseInt)
                .collect(Collectors.toList());
    }

    /**
     * Predicate shows intersection between list of stations on route and set points od departure and arrival
     */
    public static Predicate<List<Integer>> isIntersected(Integer departureID, Integer arrivalID) {
        return l -> Stream.of(departureID, arrivalID)
                .allMatch(l::contains);
    }
}
