package com.petrovsky.rs.service;

import com.petrovsky.rs.conf.properties.Properties;
import com.petrovsky.rs.model.Response;
import com.petrovsky.rs.service.functional.Functions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * RouteService
 * Service that handles all web requests from route controller and do application business logic
 *
 * @author Sergey Petrovsky
 */
@Service
public class RouteService {

    @Autowired
    private Properties properties;

    @Autowired
    private ApplicationContext applicationContext;

    public Response getDirectBusRoute(Integer departureID, Integer arrivalID) {

        List<List<Integer>> list = new ArrayList<>();
        Stream<String> stream;
        try {
            stream = new BufferedReader(new InputStreamReader(
                    applicationContext.getResource(properties.getFilePath()).getInputStream())).lines();

            list = stream
                    .map(Functions.getRouteParser())
                    .peek(l -> l.remove(0))
                    .filter(Functions.isIntersected(departureID, arrivalID))
                    .collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        }

        return new Response(departureID , arrivalID, !list.isEmpty());
    }
}
